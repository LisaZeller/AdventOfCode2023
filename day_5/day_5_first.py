import re
import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))


def get_lowest_location():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    maps = [[], [], [], [], [], [], []]

    count = -1
    for num, line in enumerate(lines):
        if num == 0:
            seeds = [int(value) for value in re.findall(r'[\d]+', line)]
        else:
            if line == '\n':
                count += 1
            elif 'map' in line:
                next
            else:
                values = line.split()
                maps[count].append([int(value) for value in values])
    
    for map in maps:
        for num, value in enumerate(seeds):
            seeds[num] = check_value(value, map)

    return min(seeds)


def check_value(value: int, translation_map):
    for tuple in translation_map:
        if tuple[1] <= value <= tuple[1] + tuple[2] - 1:
            return tuple[0] + (value - tuple[1])
    
    return value

if __name__ == '__main__':
    result = get_lowest_location()
    print(f'Lowest Location is: {result}')
