import re
import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))


def get_lowest_location():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    maps = [[], [], [], [], [], [], []]

    count = -1
    seeds = []
    for num, line in enumerate(lines):
        if num == 0:
            seed_ranges = [int(value) for value in re.findall(r'[\d]+', line)]
            for i in range(int(len(seed_ranges)/2)):
                seeds.append([seed_ranges[i*2], seed_ranges[i*2] + seed_ranges[i*2+1] - 1])
        else:
            if line == '\n':
                count += 1
            elif 'map' in line:
                next
            else:
                values = line.split()
                maps[count].append([int(value) for value in values])
    
    for map in maps:
        for num, seed in enumerate(seeds):
            for tuple in map:
                # begin and end of range are in source range
                if tuple[1] <= seed[0] and seed[1] <= tuple[1] + tuple[2] - 1:
                    seeds[num][0] = tuple[0] + (seed[0] - tuple[1])
                    seeds[num][1] = tuple[0] + (seed[1] - tuple[1])
                    break

                # only begin is in source range, split range
                elif tuple[1] <= seed[0] <= tuple[1] + tuple[2] - 1:
                    seeds.append([tuple[1] + tuple[2], seed[1]])
                    seeds[num][0] = tuple[0] + (seed[0] - tuple[1])
                    seeds[num][1] = tuple[0] + tuple[2] - 1
                    break

                # only end is in source range, split range
                elif tuple[1] <= seed[1] <= tuple[1] + tuple[2] - 1:
                    seeds.append([seed[0], tuple[1] - 1])
                    seeds[num][0] = tuple[0]
                    seeds[num][1] = tuple[0] + (seed[1] - tuple[1])
                    break
            
    return min(min(seeds))

if __name__ == '__main__':
    result = get_lowest_location()
    print(f'Lowest Location for seed ranges is: {result}')
