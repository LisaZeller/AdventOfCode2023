import re
import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))

def replace_all_number_words(line):
    line = line.replace("one", "o1e")
    line = line.replace("two", "t2o")
    line = line.replace("three", "t3e")
    line = line.replace("four", "f4r")
    line = line.replace("five", "f5e")
    line = line.replace("six", "s6x")
    line = line.replace("seven", "s7n")
    line = line.replace("eight", "e8t")
    line = line.replace("nine", "n9e")
    return line
    

def sum_calibration_values():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    sum = 0
    for line in lines:
        line = replace_all_number_words(line)

        numbers = re.findall(r'\d', line)
        sum += int(numbers[0] + numbers[-1])

    return sum

if __name__ == '__main__':
    result = sum_calibration_values()
    print(f'Sum of calibration values: {result}')
