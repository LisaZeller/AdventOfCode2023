import re
import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))


def sum_calibration_values():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    sum = 0
    for line in lines:
        numbers = re.findall(r'\d', line)
        sum += int(numbers[0] + numbers[-1])

    return sum

if __name__ == '__main__':
    result = sum_calibration_values()
    print(f'Sum of calibration values: {result}')
