import functools
import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))


def find_games_which_beat_the_record():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    five_same_cards = []
    four_same_cards = []
    fullhouse = []
    three_same_cards =  []
    two_pair = []
    one_pair = []
    high_card = []

    for line in lines:
        cards = line.split(' ')[0]
        distinct_cards = len(set(cards))
        if distinct_cards == 5:
            high_card.append(line)
        elif distinct_cards == 4:
            one_pair.append(line)
        elif distinct_cards == 3:
            if cards.count(cards[0]) in [1, 3] and cards.count(cards[1]) in [1, 3]:
                three_same_cards.append(line)
            else:
                two_pair.append(line)
        elif distinct_cards == 2:
            if cards.count(cards[0]) in [2, 3]:
                fullhouse.append(line)
            else:
                four_same_cards.append(line)
        elif distinct_cards == 1:
            five_same_cards.append(line)

    sorted_five_same_cards = sorted(five_same_cards, key=functools.cmp_to_key(compare))        
    sorted_four_same_cards = sorted(four_same_cards, key=functools.cmp_to_key(compare))  
    sorted_fullhouse = sorted(fullhouse, key=functools.cmp_to_key(compare))              
    sorted_three_same_cards = sorted(three_same_cards, key=functools.cmp_to_key(compare))        
    sorted_two_pair = sorted(two_pair, key=functools.cmp_to_key(compare))        
    sorted_one_pair = sorted(one_pair, key=functools.cmp_to_key(compare))        
    sorted_high_card = sorted(high_card, key=functools.cmp_to_key(compare))

    sorted_cards = []
    sorted_cards.extend(sorted_five_same_cards)
    sorted_cards.extend(sorted_four_same_cards)
    sorted_cards.extend(sorted_fullhouse)
    sorted_cards.extend(sorted_three_same_cards)  
    sorted_cards.extend(sorted_two_pair)
    sorted_cards.extend(sorted_one_pair)
    sorted_cards.extend(sorted_high_card)

    sorted_cards_reverse = reversed(sorted_cards)

    sum = 0
    for num, hand in enumerate(sorted_cards_reverse):
        value = int(hand.split(' ')[1])
        sum += value * (num + 1)
    return sum

def compare(x, y):
    for num in range(len(x)):
        index_x = ['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2'].index(x[num])
        index_y = ['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2'].index(y[num])
        if index_x > index_y:
            return 1
        elif index_x < index_y:
            return -1
    return 0
        
if __name__ == '__main__':
    result = find_games_which_beat_the_record()
    print(f'Multiplied games which beat the record: {result}')
