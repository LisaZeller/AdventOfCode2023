import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))


def sum_of_winning_numbers():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    sum = 0

    for line in lines:
        numbers = line.split(':')[1].split('|')
        winning_numbers = numbers[0].split()
        own_numbers = numbers[1].split()

        count = -1
        for number in winning_numbers:
            if number in own_numbers:
                count += 1

        if count > -1:
            sum += 2**count
    
    return sum

if __name__ == '__main__':
    result = sum_of_winning_numbers()
    print(f'Sum of winning numbers: {result}')
