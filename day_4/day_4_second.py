import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))


def count_scratchcards():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    scratchcards = [1 for i in range(len(lines))]

    for num, line in enumerate(lines):
        numbers = line.split(':')[1].split('|')
        winning_numbers = numbers[0].split()
        own_numbers = numbers[1].split()

        count = num
        multiplier = scratchcards[num]
        for number in winning_numbers:
            if number in own_numbers:
                count += 1
                scratchcards[count] += 1 * multiplier
    
    return sum(scratchcards)

if __name__ == '__main__':
    result = count_scratchcards()
    print(f'Sum of scratchcards: {result}')
