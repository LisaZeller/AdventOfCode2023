import re
import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))

def sum_of_adjacent_numbers():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    sum = 0
    length_line = len(lines[0])
    length_lines = len(lines)

    for num, line in enumerate(lines):
        line = line.strip()
        # find number and pos, give attention to numbers in numbers like 1, 2, ...
        numbers = re.findall(r'\d+', line)
        start_pos = 0
        end_pos = 0
        for number in numbers:
            start_pos = (line[end_pos:]).find(number) + end_pos
            end_pos = start_pos + len(number)

            # expand start and end
            if start_pos != 0:
                start_pos -= 1
            if end_pos != length_line:
                end_pos += 1

            # check lines for a symbol
            symbols_actual = re.sub(r'\.*\d*', "", line[start_pos:end_pos])
            symbols_above = ''
            symbols_below = ''

            if num != 0:
                symbols_above = re.sub(r'\.*\d*', "", lines[num-1][start_pos:end_pos])
            if num != length_lines-1:
                symbols_below = re.sub(r'\.*\d*', "", lines[num+1][start_pos:end_pos])

            if symbols_actual or symbols_above.strip() or symbols_below.strip():
                sum += int(number)
            

    return sum

if __name__ == '__main__':
    result = sum_of_adjacent_numbers()
    print(f'Sum of adjacent numbers: {result}')
