import re
import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))

def sum_of_adjacent_numbers_to_stars():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    sum = 0
    length_line = len(lines[0])
    length_lines = len(lines)

    all_numbers = []
    for line in lines:
        all_numbers.append([[m.start(0),m.end(0)-1, int(m.group(0))] for m in re.finditer("\d+", line)])
    
    for num, line in enumerate(lines):
        line = line.strip()
        # find all *
        stars = len(re.findall(r'\*', line))
        start_pos = 0
        end_pos = 0
        for i in range(stars):
            numbers = []
            middle = (line[end_pos:]).find('*') + end_pos

            # expand start and end
            if middle != 0:
                start_pos = middle - 1
            if middle != length_line:
                end_pos = middle + 1

            # check actual line
            for value in all_numbers[num]:
                if start_pos == value[1] or end_pos == value[0]:
                    numbers.append(value[2])

            # check line above
            if num != 0:
                for value in all_numbers[num-1]:
                    if start_pos <= value[0] <= end_pos or start_pos <= value[1] <= end_pos:
                        numbers.append(value[2])

            # check line below
            if num != length_lines -1:
                for value in all_numbers[num+1]:
                    if start_pos <= value[0] <= end_pos or start_pos <= value[1] <= end_pos:
                        numbers.append(value[2])

            if len(numbers) == 2:
                sum+= numbers[0] *numbers[1]

    return sum

if __name__ == '__main__':
    result = sum_of_adjacent_numbers_to_stars()
    print(f'Sum of adjacent numbers: {result}')
