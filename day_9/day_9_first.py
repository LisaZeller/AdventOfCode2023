import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))


def find_sum_of_next_values():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    total = 0
    for line in lines:
        # find zeros
        values = [int(value) for value in line.strip().split(' ')]
        last_items = [values[-1]]
        diff_values = values
        # give attention to arrays which are in sum zero but not all elements
        while (sum(diff_values) != 0 or diff_values[0] != 0):
            new_diff_values = []
            for i in range(len(diff_values)-1):
                new_diff_values.append(diff_values[i+1]- diff_values[i])

            last_items.append(new_diff_values[-1])
            diff_values = new_diff_values

        # add values
        total += sum(last_items)

    return total
        
if __name__ == '__main__':
    result = find_sum_of_next_values()
    print(f'Sum of next values: {result}')
