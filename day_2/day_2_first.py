import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))

def sum_of_possible_games():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    sum = 0
    for num, line in enumerate(lines):
        max_red = 0
        max_blue = 0
        max_green = 0
        all_revealed = line.split(':')[1].replace(',', '').replace(';', '').split()
        for i in range(int(len(all_revealed)/2)):
            color = all_revealed[i*2 + 1]
            number = int(all_revealed[i*2])
            if color == 'red' and number > max_red:
                max_red = number
            if color == 'blue' and number > max_blue:
                max_blue = number
            if color == 'green' and number > max_green:
                max_green = number
                
        if max_red <= 12 and max_blue <= 14 and max_green <= 13:
            sum += num+1

    return sum

if __name__ == '__main__':
    result = sum_of_possible_games()
    print(f'Sum of possible games: {result}')
