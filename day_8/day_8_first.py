import re
import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))


def find_shortest_way_to_ZZZ():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    instructions = ""
    nodes = {}

    for num, line in enumerate(lines):
        if num == 0:
            instructions = line.strip()
        elif num > 1:
            splitted_line = re.findall(r'\b\w+\b', line)
            nodes[splitted_line[0]] = [splitted_line[1], splitted_line[2]]

    count = 0
    actual_node = 'AAA'
    for i in range(100):
        for direction in instructions:
            count += 1
            if direction == 'R':
                actual_node = nodes.get(actual_node)[1]
            else:
                actual_node = nodes.get(actual_node)[0]

            if actual_node == 'ZZZ':
                return count
    return -1
        
if __name__ == '__main__':
    result = find_shortest_way_to_ZZZ()
    print(f'Shortest way to ZZZ: {result}')
