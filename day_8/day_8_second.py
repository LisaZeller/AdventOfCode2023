import re
import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))


def find_shortest_way_to_ZZZ():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    instructions = ""
    nodes = {}

    for num, line in enumerate(lines):
        if num == 0:
            instructions = line.strip()
        elif num > 1:
            splitted_line = re.findall(r'\b\w+\b', line)
            nodes[splitted_line[0]] = [splitted_line[1], splitted_line[2]]

    all_Z_positions = []
    first_nodes = [key for key in nodes.keys() if key[-1] == 'A']

    for node in first_nodes:
        count = 0
        Z_positions = []
        actual_node = node
        start_nodes = []

        set_break = False
        for i in range(100):
            for num, direction in enumerate(instructions):
                count += 1
                if num ==  0:
                    if actual_node in start_nodes:
                        all_Z_positions.append(Z_positions)
                        set_break = True
                        break
                    else:
                        start_nodes.append(actual_node)
                
                if direction == 'R':
                    actual_node = nodes.get(actual_node)[1]
                else:
                    actual_node = nodes.get(actual_node)[0]

                if actual_node[-1] == 'Z':
                    Z_positions.append(count)
            if set_break:
                break

    multipliers = []

    for value in all_Z_positions:
        for i in range(2, value[0], 1):
            if value[0] % i == 0:
                multipliers.append(i)

    result = 1
    for x in set(multipliers):
        result = result * x

    return result

        
if __name__ == '__main__':
    result = find_shortest_way_to_ZZZ()
    print(f'Shortest way from all A to all Z: {result}')
