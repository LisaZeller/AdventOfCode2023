import re
import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))


def find_games_which_beat_the_record():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    times = [int(value) for value in re.findall(r'[\d]+', lines[0])]
    records = [int(value) for value in re.findall(r'[\d]+', lines[1])]

    multiplied_beats = 1

    for time, record in zip(times, records):
        count = 0
        for i in range(time):
            distance = i*(time-i)
            if distance > record:
                count += 1
        multiplied_beats *= count

    return multiplied_beats

if __name__ == '__main__':
    result = find_games_which_beat_the_record()
    print(f'Multiplied games which beat the record: {result}')
