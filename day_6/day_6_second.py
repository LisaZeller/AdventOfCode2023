import re
import os

LOCAL_FOLDER = os.path.dirname(os.path.realpath(__file__))


def find_games_which_beat_the_record():
    with open(os.path.join(LOCAL_FOLDER, 'input.txt')) as file:
        lines = file.readlines()

    time = int(re.findall(r'[\d]+', lines[0].replace(' ', ''))[0])
    record = int(re.findall(r'[\d]+', lines[1].replace(' ', ''))[0])

    start_beating = 0
    end_beating = 0

    for i in range(time):
        distance = i*(time-i)
        if distance > record:
            start_beating = i
            break
    
    for i in range(time, 0, -1):
        distance = i*(time-i)
        if distance > record:
            end_beating = i + 1
            break

    return end_beating - start_beating

if __name__ == '__main__':
    result = find_games_which_beat_the_record()
    print(f'Total games which beat the record: {result}')
